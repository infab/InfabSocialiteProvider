<?php

namespace Infab\InfabSocialiteProvider;

use SocialiteProviders\Manager\SocialiteWasCalled;

class InfabExtendSocialite
{
    /**
     * Execute the provider.
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('infab', __NAMESPACE__.'\Provider');
    }
}
