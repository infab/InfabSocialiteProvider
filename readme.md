# Infab Socialite Provider

v1.0.7


Add the provider in `app/Providers/EventServiceProvider.php`:

```php
    protected $listen = [
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'Infab\InfabSocialiteProvider\InfabExtendSocialite@handle',
        ],
    ];
```

Add infab to the services array in `config/services.php`:

```php
    'infab' => [
        'client_id' => env('INFAB_CLIENT_ID'),         // Your Infab Client ID
        'client_secret' => env('INFAB_CLIENT_SECRET'), // Your Infab Client Secret
        'redirect' => 'http://shin.test/login/infab/callback'
    ],

```
*Note that the redirect has to match with the configured redirect path in the OAuth server (auth.infab.nu)*
